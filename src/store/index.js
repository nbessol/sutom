import { createStore } from "vuex";

import { getRandomWord } from "../lib/data";

export default createStore({
  state: {
    randomWord: "",
  },

  getters: {
    word(state) {
      return state.randomWord;
    },
  },

  mutations: {
    setMainWord(state, payload) {
      state.randomWord = payload;
    },
  },

  actions: {
    async getMainWord({ commit }) {
      const randomWord = await getRandomWord();
      commit("setMainWord", randomWord);
    },
  },
});
