import randomword from "random-words";

export async function getRandomWord() {
  const randomWord = new Promise(function (resolve, reject) {
    const myWords = randomword(10);
    const myWord = myWords.find((el) => el.length > 5 && el.length < 10);
    resolve(myWord);
    reject(randomWord());
  });
  const word = await randomWord;
  return word;
}
