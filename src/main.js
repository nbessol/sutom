import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "./assets/styles/reset.css";
import "./assets/styles/default.css";

createApp(App).use(router).use(store).mount("#app");
